import { MarchingSquareHelper } from "./MarchingSquareHelper";


abstract class MarchingSquareState {
    /**
     * 
     * @param topLeft evaluated function at top-left corner
     * @param topRight evaluated function at top-right corner
     * @param bottomLeft evaluated function at bottom-left corner
     * @param bottomRight evaluated function at bottom-right corner
     * @param target the function target value. values of the corners below are deemed "outside", values above are deemed "inside"
     * @returns a State between 0 (inclusive) and 15 (inclusive). 
     *          Each bit represents a corner. If the value at the corner is above the target, 
     *          this corner is defined as inside, and gets a true bit. Else, a false bit is set.
     */
    private static getSquareState(topLeft: number, topRight: number, bottomLeft: number, bottomRight: number, target: number) {
        let value = 0b0000;

        if(topLeft > target) {
            value |= 0b1000;
        }
        if(topRight > target) {
            value |= 0b0100;
        }
        if(bottomLeft > target) {
            value |= 0b0010;
        }
        if(bottomRight > target) {
            value |= 0b0001;
        }
        return value;
    }

    /**
     * Returns an Array of Vec2s representing the poly-line to be drawn.
     * This method is expected to be called twice: with a filling pass, and a stroke pass (assuming filling is enabled).
     * 
     * @param minX lower Bound of X to be mapped to 0
     * @param maxX upper Bound of X to be mapped to 1
     * @param minY lower Bound of Y to be mapped to 0
     * @param maxY upper Bound of Y to be mapped to 1
     * @param func the evaluating function, taking two parameters, x and y
     * @param target The line at which the transition between "inside" and "outside" is determined.
     */
    public static approximateOnUnitSquare(minX: number, maxX: number, minY: number, maxY: number, func: (x:number, y: number) => number, target: number ): [number, number][]{
        const cornerValues = this.evaluateCorners(minX, maxX, minY, maxY, func);
        const state = this.getSquareState(cornerValues.topLeft, cornerValues.topRight, cornerValues.bottomLeft, cornerValues.bottomRight, target);

        if(state === 0b0000) {
            // No corners are inside. Skip
            return []
        }
        let vectors: [number, number][];
        if(state === 0b1111) {
            // All corners are inside.
             vectors = [
                [0, 0],
                [0, 1],
                [1, 1],
                [1, 0],
                [0, 0]
            ]
        }
        else if([0b1000, 0b0100, 0b0010, 0b0001].some(e => e === state)) {
            // One corner is inside
            vectors = MarchingSquareHelper.handleOneCornerInside(state, target, cornerValues);
        }
        else if([0b1100, 0b0101, 0b0011, 0b1010].some(e => e === state)){
            // Two corners are inside. No intersection.
            vectors = MarchingSquareHelper.handleTwoCornersInside(state, target, cornerValues);
        }
        else if([0b0111, 0b1011, 0b1101, 0b1110].some(e => e === state)) {
            // Three Corners are inside
            vectors = MarchingSquareHelper.handleThreeCornersInside(state, target, cornerValues);
        }
        else {
            console.warn(`Received an unsupported state ${state}. Ignoring.`)
            return []
        }

        return vectors;
    }

    private static evaluateCorners(minX: number, maxX: number, minY: number, maxY: number, func: (x: number, y: number) => number) {
        const topLeft = func(minX, maxY);
        const topRight = func(maxX, maxY);
        const bottomLeft = func(minX, minY);
        const bottomRight = func(maxX, minY);

        return { topLeft, topRight, bottomLeft, bottomRight }
    }
}

export {MarchingSquareState}