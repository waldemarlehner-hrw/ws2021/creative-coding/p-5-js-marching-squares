import p5 from "p5";
import { MarchingSquareState } from "./MarchingSquareState";
import { Settings } from "./Settings";

class MarchingSquaresContext {
    private p5: p5;
    
    constructor(p5Ctx: p5, private settings: Settings, ) {
        this.p5 = p5Ctx;
    }
    
    public drawFunction(func: (x: number, y: number) => number, target: number) {
        for(let x = 0; x < this.settings.xCount; x++) {
            for(let y = 0; y < this.settings.yCount; y++) {
        
                const leftBoundInFunction = this.settings.xPosOffset + (x/this.settings.xCount) * this.settings.stepPerTile;
                const rightBoundInFunction = this.settings.xPosOffset + ((x+1)/this.settings.xCount) * this.settings.stepPerTile;
                const bottomBoundInFunction = this.settings.yPosOffset + (y/this.settings.yCount) * this.settings.stepPerTile;
                const topBoundInFunction = this.settings.yPosOffset + ((y+1)/this.settings.yCount) * this.settings.stepPerTile;
                
                const shapeToDrawOnUnit = MarchingSquareState.approximateOnUnitSquare(leftBoundInFunction, rightBoundInFunction, bottomBoundInFunction, topBoundInFunction, func, target);
                const xTileSize = (this.settings.screenResolution / this.settings.xCount);
                const yTileSize = (this.settings.screenResolution / this.settings.yCount);
    

                this.p5.beginShape()
                for(const position of shapeToDrawOnUnit) {
                    const canvasX = (x+position[0]) * xTileSize;
                    const canvasY = (y+position[1]) * yTileSize;
                    this.p5.vertex(canvasX, canvasY)
                }
                this.p5.endShape();
                
            }
        }
    }
}

export {MarchingSquaresContext}