import p5 from "p5";
import { MarchingSquaresContext } from "./MarchingSquaresContext";
import { Settings } from "./Settings";

class MarchingSquaresSketch {
	
	private canvas?: p5.Renderer;
	private screenSize = 800;
	private squaresCtx: MarchingSquaresContext;
	private settings: Settings;
	private fpsGraph: any;
	private animationCounter = 0;
	updateUiCallback: () => void;

	constructor(private p: p5){
		p.setup = () => this.setup();
		p.draw = () => this.draw();
		this.settings = new Settings();
		this.squaresCtx = new MarchingSquaresContext(p, this.settings)
		let [fpsGraph, updateUi] = Settings.attachAsTweakpane(this.settings)
		this.fpsGraph = fpsGraph;
		this.updateUiCallback = updateUi as any;
	}

	private setup() {
		this.p.noSmooth()
		this.canvas = this.p.createCanvas(this.screenSize, this.screenSize);
		this.canvas.parent("app");
		this.p.strokeWeight(1.4)
		this.p.strokeJoin("round")
	}

	private draw() {
		this.fpsGraph.begin()
		this.p.background(this.settings.backgroundColor);
		this.p.fill(this.settings.foregroundColor)
		this.settings.drawOutline ? this.p.stroke(0) : this.p.stroke(this.settings.foregroundColor);
		this.squaresCtx.drawFunction((x: number, y: number) => {
			return (Math.sin(x) + Math.sin(y));
		}, this.settings.target);
		if(this.settings.animateTarget){
			this.animationCounter++;
			this.settings.target = Math.sin(this.animationCounter / 100)
			this.updateUiCallback()
		}
		this.fpsGraph.end();
	}
	
	public static toP5Sketch(): (p5: p5) => void {
		return (p5: p5) => new MarchingSquaresSketch(p5);
	}
}

export default MarchingSquaresSketch;
