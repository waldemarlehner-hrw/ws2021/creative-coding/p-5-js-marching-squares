enum Edge {
    Top, Left, Right, Bottom
}


abstract class MarchingSquareHelper {
    public static handleThreeCornersInside(state: number, target: number, cornerValues: { topLeft: number; topRight: number; bottomLeft: number; bottomRight: number; }): [number, number][] {
        if(state === 0b0111) {
            // TopLeft
      
            const topEdgeX = this.evaluateEdge(Edge.Top, target, cornerValues);
            const leftEdgeY = this.evaluateEdge(Edge.Left, target, cornerValues);
           
            return [
                [topEdgeX, 1], [1,1], [1,0],[0,0],[0, leftEdgeY], [topEdgeX, 1]
            ];
        }
        else if(state === 0b1011) {
            // TopRight
          
            const topEdgeX = this.evaluateEdge(Edge.Top, target, cornerValues);
            const rightEdgeY = this.evaluateEdge(Edge.Right, target, cornerValues);
            return [
                [0, 1], [topEdgeX, 1], [1, rightEdgeY], [1, 0], [0, 0], [0, 1]
            ]
        }
        else if(state === 0b1101) {
            // BottomLeft
   
            const bottomEdgeX = this.evaluateEdge(Edge.Bottom, target, cornerValues);
            const leftEdgeY = this.evaluateEdge(Edge.Left, target, cornerValues);
            return [
                [0, leftEdgeY], [0, 1], [1,1], [1, 0], [bottomEdgeX, 0], [0, leftEdgeY]
            ]            
        }
        else{
           
            // BottomRight
            const bottomEdgeX = this.evaluateEdge(Edge.Bottom, target, cornerValues);
            const rightEdgeY = this.evaluateEdge(Edge.Right, target, cornerValues);
            return [
                [0, 0], [0, 1], [1, 1], [1, rightEdgeY], [bottomEdgeX, 0], [0, 0]
            ]
            
        }
    }

    public static handleTwoCornersInside(state: number, target: number, cornerValues: { topLeft: number; topRight: number; bottomLeft: number; bottomRight: number; }): [number, number][] {
        if(state === 0b1100 || state === 0b0011) {
            const yInsideFilledRegion = state === 0b1100 ? 1 : 0; // if true: fill top, else, fill bottom
            const leftEdgeY = this.evaluateEdge(Edge.Left, target, cornerValues);
            const rightEdgeY = this.evaluateEdge(Edge.Right, target, cornerValues);
            return [
                [0, leftEdgeY],
                [0, yInsideFilledRegion],
                [1, yInsideFilledRegion],
                [1, rightEdgeY],
                [0, leftEdgeY],
            ]
        }
        else if(state === 0b1010 || state === 0b0101) {
            const xInsideFilledRegion = state === 0b1010 ? 0 : 1 // if true: fill left, else, fill right
            const topEdgeX = this.evaluateEdge(Edge.Top, target, cornerValues);
            const bottomEdgeX = this.evaluateEdge(Edge.Bottom, target, cornerValues);
            return [
                [xInsideFilledRegion, 1],
                [topEdgeX, 1],
                [bottomEdgeX, 0],
                [xInsideFilledRegion, 0],
                [xInsideFilledRegion, 1]
            ]
        }
        return []
    }
    public static handleOneCornerInside(state: number, target: number, cornerValues: { topLeft: number; topRight: number; bottomLeft: number; bottomRight: number; }): [number, number][] {
        if(state === 0b1000) {
            // TopLeft
            const topEdgeX = this.evaluateEdge(Edge.Top, target, cornerValues);
            const leftEdgeY = this.evaluateEdge(Edge.Left, target, cornerValues);
            return [
                [0, leftEdgeY],
                [0, 1],
                [topEdgeX, 1],
                [0, leftEdgeY]
            ]
            //return [[topEdgeX, 1], [1,1], [1,0],[0,0],[0, leftEdgeY], [topEdgeX, 1]].map(([x,y]) => new p5.Vector().add(x,y));
        }
        else if(state === 0b0100) {
            // TopRight
            const topEdgeX = this.evaluateEdge(Edge.Top, target, cornerValues);
            const rightEdgeY = this.evaluateEdge(Edge.Right, target, cornerValues);
            return [
                [topEdgeX, 1],
                [1,1],
                [1, rightEdgeY],
                [topEdgeX, 1]
            ]
        }
        else if(state === 0b0010) {
            // BottomLeft
            const bottomEdgeX = this.evaluateEdge(Edge.Bottom, target, cornerValues);
            const leftEdgeY = this.evaluateEdge(Edge.Left, target, cornerValues);
            return [
                [bottomEdgeX, 0],
                [0, 0],
                [0, leftEdgeY],
                [bottomEdgeX, 0],
            ]
        }
        else{
            // BottomRight
            const bottomEdgeX = this.evaluateEdge(Edge.Bottom, target, cornerValues);
            const rightEdgeY = this.evaluateEdge(Edge.Right, target, cornerValues);
             return [
                 [1, rightEdgeY],
                 [1, 0],
                 [bottomEdgeX, 0],
                 [1, rightEdgeY]
             ]
        }
    }
    
    private static evaluateEdge(edge: Edge, target: number, cornerValues: { topLeft: number; topRight: number; bottomLeft: number; bottomRight: number; }) {
        if(edge === Edge.Top) {
            const xFraction = this.getLinearNormalizedPosition(cornerValues.topLeft, cornerValues.topRight, target);
            return xFraction;
        }
        else if(edge === Edge.Bottom) {
            const xFraction = this.getLinearNormalizedPosition(cornerValues.bottomLeft, cornerValues.bottomRight, target);
            return xFraction;

        }
        else if(edge === Edge.Left) {
            const yFraction = this.getLinearNormalizedPosition(cornerValues.bottomLeft, cornerValues.topLeft, target);
            return yFraction;
        }
        else if(edge === Edge.Right) {
            const yFraction = this.getLinearNormalizedPosition(cornerValues.bottomRight, cornerValues.topRight, target);
            return yFraction;
        }
        else {
            throw new Error("Unexpected Enum value:" + edge)
        }
    }

    private static getLinearNormalizedPosition(lowerBoundMappedToZero: number, upperBoundMappedToOne: number, target: number) {
        
        if(lowerBoundMappedToZero < upperBoundMappedToOne) {
            if(target < lowerBoundMappedToZero || target > upperBoundMappedToOne) {
                throw new Error(`Target is outside the bounds. Bounds: [${lowerBoundMappedToZero}, ${upperBoundMappedToOne}], value: ${target}`);   
            }
        }

        if(upperBoundMappedToOne < lowerBoundMappedToZero) {
            if(target > lowerBoundMappedToZero || target < upperBoundMappedToOne) {
                throw new Error(`Target is outside the bounds. Bounds: [${lowerBoundMappedToZero}, ${upperBoundMappedToOne}], value: ${target}`);     
            }
        }


        return (target - lowerBoundMappedToZero)/(upperBoundMappedToOne-lowerBoundMappedToZero);

    }

}


export {Edge, MarchingSquareHelper}