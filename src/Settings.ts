import { Pane } from "tweakpane";
import * as EssentialsPlugin from "@tweakpane/plugin-essentials"

class Settings {
    screenResolution: number = 800;

    xCount: number = 13
    yCount: number = 13

    xPosOffset: number = 0
    yPosOffset: number = 0

    stepPerTile: number = 20

	animateTarget: boolean = true
    target: number = 0.6;

    drawOutline: boolean = true
    backgroundColor: string = "#EAEAEA"
    foregroundColor: string = "#AA4019"

    __fnText = "sin(x)+sin(y)"

    public static attachAsTweakpane(settings: Settings) {
        const pane = new Pane();
        pane.registerPlugin(EssentialsPlugin)
        const fpsGraph = pane.addBlade({view: "fpsgraph"})
        const fnFolder = pane.addFolder({
            title: "Function"
        })
        fnFolder.addInput(settings, "__fnText", {label: "fn(x,y)=", disabled: true})
        fnFolder.addInput(settings, "animateTarget")
        fnFolder.addInput(settings, "target", {"step": 0.01})
        fnFolder.addInput(settings, "stepPerTile", {"min": 0})
        fnFolder.addInput(settings, "xPosOffset")
        fnFolder.addInput(settings, "yPosOffset")
        
        const marchingSquaresFolder = pane.addFolder({title: "Marching Squares Config"})
        marchingSquaresFolder.addInput(settings, "xCount", {"step": 1, "min":1})
        marchingSquaresFolder.addInput(settings, "yCount", {"step": 1, "min": 1})

        const rendererFolder = pane.addFolder({title: "Renderer"})
        rendererFolder.addInput(settings, "drawOutline")
        rendererFolder.addInput(settings, "backgroundColor")
        rendererFolder.addInput(settings, "foregroundColor")

        return [fpsGraph, () => pane.refresh()];
    }

}

export {Settings}