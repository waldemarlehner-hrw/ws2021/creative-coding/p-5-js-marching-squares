import './style.css'
import P5 from "p5";
import MarchingSquaresSketch from "./MarchingSquaresSketch"

const sketch = MarchingSquaresSketch.toP5Sketch();

new P5(sketch);
